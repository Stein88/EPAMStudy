package com.emap.study.StringTask;

import java.util.List;
import java.util.Map;

/**
 * Created by Stein on 23.11.16.
 */
public interface StringModifier {

    int printCharCount(String str0);

    boolean compareStrings(String str0, String str1);

    String toUpperCase(String str0);

    String toLowerCase(String str0);

    List<Integer> getIndexes(String str0 , String key);

    String replace(String str0, String key, String replace);

    Map<String, Integer> findEqualWord(String str0);


}
