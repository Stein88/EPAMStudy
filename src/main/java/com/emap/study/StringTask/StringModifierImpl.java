package com.emap.study.StringTask;


import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Stein on 23.11.16.
 */
public class StringModifierImpl implements StringModifier {


    public int printCharCount(String str0) {
        if (str0 == null || str0.isEmpty()) {
            return -1;
        }
        int count = 0;
        for (char c : str0.toCharArray()) {
            if (Character.isLetterOrDigit(c)) {
                count++;
            }
        }
        return count;
    }

    public boolean compareStrings(String str0, String str1) {
        if (str0 == null || str0.isEmpty()) {
            return false;
        }
        return str0.equalsIgnoreCase(str1);
    }

    public String toUpperCase(String str0) {
        if (str0 == null || str0.isEmpty()) {
            return "Create correct string";
        }
        return str0.toUpperCase();
    }

    public String toLowerCase(String str0) {
        if (str0 == null || str0.isEmpty()) {
            return "Create correct string";
        }
        return str0.toLowerCase();
    }

    public List<Integer> getIndexes(String str0, String key) {
        List<Integer> indexesList = new ArrayList<>();
        Matcher matcher = Pattern.compile(key).matcher(str0);
        while (matcher.find()) {
            for (int i = matcher.start(); i < matcher.end(); i++) {
                indexesList.add(i);
            }
        }
        return indexesList;
    }

    public String replace(String str0, String key, String replace) {
        return str0.replaceAll(key, replace);
    }

    public Map<String, Integer> findEqualWord(String str0) {
        HashMap<String, Integer> map = new HashMap<>();

        for(String match : str0.split(" ")) {
            if(map.putIfAbsent(match, 1) != null) {
                map.put(match, map.get(match) + 1);
            }
        }
        return map;
    }
}
