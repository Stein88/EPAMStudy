package com.emap.study.StringTask;

/**
 * Created by Stein on 29.11.16.
 */
public class StringRunner {

    public static void main(String[] args) {
        StringModifier stringModifier = new StringModifierImpl();
        String str0 = "bng dong bong";
        String str1 = "bng dong bong";
        String key = "n";
        String replace = "bong";
        System.out.println(stringModifier.printCharCount(str0));
        System.out.println(stringModifier.compareStrings(str0, str1));
        System.out.println(stringModifier.toUpperCase(str0));
        System.out.println(stringModifier.toLowerCase(str0));
        System.out.println(stringModifier.getIndexes(str0, key));
        System.out.println(stringModifier.replace(str0, key, replace));
        System.out.println(stringModifier.findEqualWord(str0));







    }
}
