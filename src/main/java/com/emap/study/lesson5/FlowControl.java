package com.emap.study.lesson5;

import java.util.List;

/**
 * Created by Stein on 09.11.16.
 */
public class FlowControl {
    public static final double PERCENT = 100;

    public static StringBuilder printTableA(int a) {
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i <= a; i++) {
            for (int j = i; j >= 0; j--) {
                temp.append(j + " ");
            }
            temp.append('\n');
        }
        return temp;
    }

    public static StringBuilder printTableB(int a) {
        StringBuilder temp = new StringBuilder();
        for (int i = 1; i <= a; i++) {
            for (int j = a; j >= 1; j--) {
                if (j <= i) {
                    temp.append(" " + j);
                }
            }
            temp.append('\n');
        }
        return temp;
    }

    public static int count(int number, char numberOfDigit) {
        int count = 0;
        for (char el : String.valueOf(number).toCharArray()) {
            if (el == numberOfDigit)
                count++;
        }
        return count;
    }

    public static int elementCount(List<Integer> listElements) {
        int count = 0;
        for (Integer element : listElements) {
            count++;
        }
        return count;
    }

    public static int minElement(List<Integer> listElements) {
        int minElement = listElements.get(0);
        for (Integer element : listElements) {
            if (element < minElement) {
                minElement = element;
            }
        }
        return minElement;
    }

    public static int maxElement(List<Integer> listElements) {
        int maxElement = listElements.get(0);
        for (Integer element : listElements) {
            if (element > maxElement) {
                maxElement = element;
            }
        }
        return maxElement;
    }

    public static double avrElements(List<Integer> listElements) {
        double avr = 0;
        for (Integer element : listElements) {
            avr += element.doubleValue();
        }
        return avr /= listElements.size();
    }

    public static int depositWithFor(double startMoney, double startPercent, double expectedMoney) {
        int year = 0;
        for (; startMoney < expectedMoney; startMoney += startMoney * startPercent / PERCENT) {
            year++;
        }
        return year;
    }

    public static int depositDo(double startMoney, int startPercent, double expectedMoney) {
        int year = 0;
        if (startMoney < expectedMoney) {
            do {
                startMoney += startMoney * startPercent / PERCENT;
                year++;
            } while (startMoney < expectedMoney);
        }
        return year;
    }

    public static int depositWhile(double startMoney, int startPercent, double expectedMoney) {
        int year = 0;
        while (startMoney < expectedMoney) {
            startMoney += startMoney * startPercent / PERCENT;
            year++;
        }
        return year;
    }

    public static int depositWhileBreak(double startMoney, int startPercent, double expectedMoney) {
        int year = 0;
        if (startMoney < expectedMoney) {
            while (true) {
                startMoney += startMoney * startPercent / PERCENT;
                year++;
                if (startMoney >= expectedMoney)
                    break;
            }
        }
        return year;
    }

    public static int depositWhileExit(double startMoney, int startPercent, double expectedMoney) {
        int year = 0;
        if (startMoney < expectedMoney) {
            while (true) {
                startMoney += startMoney * startPercent / PERCENT;
                year++;
                if (startMoney >= expectedMoney)
                    System.exit(0);
            }
        }
        return year;
    }
}
