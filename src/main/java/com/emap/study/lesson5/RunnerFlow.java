package com.emap.study.lesson5;

import java.util.Arrays;

import static com.emap.study.lesson5.FlowControl.*;

/**
 * Created by Stein on 09.11.16.
 */
public class RunnerFlow {
    public static void main(String[] args) {
        System.out.println("Task1:");
        System.out.println(count(25344555, '0'));
        System.out.println("Task2:");
        System.out.println(elementCount(Arrays.asList(3, 4, -10, 2, 8, 1, -11)));
        System.out.println(minElement(Arrays.asList(3, 4, -10, 2, 8, 1, -11)));
        System.out.println(maxElement(Arrays.asList(3, 4, -10, 2, 8, 1, -11)));
        System.out.println(avrElements(Arrays.asList(3, 4, -10, 2, 8, 1, -11)));
        System.out.println("Task3:");
        System.out.println(depositDo(1000, 10, 1200));
        System.out.println(depositWhile(1000, 10, 1200));
        System.out.println(depositWhileBreak(1000, 10, 1200));
        //System.out.println(depositWhileExit(1000, 10, 1200));
        System.out.println(depositWithFor(1000, 10, 1200));
        System.out.println("Task4:");
        System.out.println(printTableA(4));
        System.out.println(printTableB(5));
    }
}
