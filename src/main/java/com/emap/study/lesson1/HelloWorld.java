package com.emap.study.lesson1;

/**
 * Created by Stein on 28.10.16.
 */
public class HelloWorld {
    public static void main(String[] args) {
        //byte b = (byte) 128;
        //System.out.println(b);
        byte b = 1, b1 = 1 + 2;
        b = (byte) (b1 + 1); //cast
        byte b2 = 123;
        //b = b2 + 1;
        System.out.println(b2);

        Float f1 = new Float(30.10);
        int i1 = 7;
        long i6 = 8;
        Integer i2 = i1;
        Integer i3 = i1;
        System.out.println(i1 == i2);
        System.out.println(i3 == i1);
        System.out.println(i2 == i3);
        System.out.println(i2.equals(i3));


    }
}
