package com.emap.study.inner_sample;

/**
 * Created by Stein on 09.11.16.
 */
public interface IEngine {
    void start();
    void stop();

}
