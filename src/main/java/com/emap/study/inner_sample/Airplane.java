package com.emap.study.inner_sample;


/**
 * Created by Stein on 22.11.16.
 */
public class Airplane {
    public class AirplaneEngine implements IEngine {
        private class Engine {
            private String number;

            public Engine(String number) {
                this.number = number;
            }

            @Override
            public String toString() {
                return number;
            }
        }

        private Engine engine;

        public AirplaneEngine(String number) {
            this.engine = this.new Engine(number);
        }

        public void start() {
            System.out.println("Start engine " + engine.number);
        }

        public void stop() {
            System.out.println("Stop engine " + engine.number);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Engine=").append(engine);

            return sb.toString();
        }
    }

    private String model;
    private AirplaneEngine left;
    private AirplaneEngine right;

    public Airplane(String model) {
        this.model = model;
    }

    public void fly() {
        left.start();
        right.start();
    }

    public void landing(){
        left.stop();
        right.stop();
    }

    public void setLeft(AirplaneEngine left) {
        this.left = left;
    }

    public void setRight(AirplaneEngine right) {
        this.right = right;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Airplane{");
        sb.append("model='").append(model).append('\'');
        sb.append(", left").append(left);
        sb.append(", right").append(right);
        sb.append('}');
        return sb.toString();
    }
}
