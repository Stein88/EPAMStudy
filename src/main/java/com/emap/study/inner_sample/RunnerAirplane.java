package com.emap.study.inner_sample;


/**
 * Created by Stein on 22.11.16.
 */
public class RunnerAirplane {
    public static void main(String[] args) {
        Airplane airplane = new Airplane("Airbus");
        airplane.setRight(airplane.new AirplaneEngine("1"));
        airplane.setLeft(airplane.new AirplaneEngine("2"));
        System.out.println(airplane);

        airplane.fly();
        airplane.landing();


    }
}
