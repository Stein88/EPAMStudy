package com.emap.study.inner_sample.nestet_local_anony;

/**
 * Created by Stein on 23.11.16.
 */
public class RunnerImplementation {
    public static void main(String[] args) {
        Method method = new Implementation().createMethodInner();
        method.someMethod();

        Method method2 = new Implementation().createMethodLocal();
        method2.someMethod();

        Method method3 = new Implementation().createMethodAnon();
        method3.someMethod();


    }
}
