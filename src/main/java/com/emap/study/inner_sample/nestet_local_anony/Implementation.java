package com.emap.study.inner_sample.nestet_local_anony;

/**
 * Created by Stein on 23.11.16.
 */
public class Implementation {

    private class MethodInner implements Method {
        public void someMethod() {
            System.out.println("someMethod implemented in privateInner");
        }
    }

    public MethodInner createMethodInner() {
        return new MethodInner();
    }

    public Method createMethodLocal() {
        class MethodLocal implements Method {
            public void someMethod() {
                System.out.println("someMethod implemented in local");

            }
        }
        return new MethodLocal();
    }

    public Method createMethodAnon() {
        return new Method() {
            public void someMethod() {
                System.out.println("someMethod in anon");
            }
        };
    }
}
