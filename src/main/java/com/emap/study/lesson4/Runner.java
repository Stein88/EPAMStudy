package com.emap.study.lesson4;

import java.util.ArrayList;
import java.util.List;

import static com.emap.study.lesson4.CarUtils.printCar;

/**
 * Created by Stein on 04.11.16.
 */
public class Runner {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList();
        cars.add(new Car(1L, "Toyota", "Avensis", 2015, "red", 1000L, "#21345"));
        cars.add(new Car(2L, "Opel", "Astra", 2011, "blue", 2000L, "#2135"));
        cars.add(new Car(3L, "Toyota", "Camry", 2010, "red", 1500L, "#21245"));
        cars.add(new Car(4L, "BA3", "2106", 2006, "red", 1600L, "#21445"));
        cars.add(new Car(5L, "Toyota", "Avensis", 2010, "red", 2100L, "#21345"));

        CarPark park = new CarPark();
        park.registerCar(cars);
        printCar(park.carsOfYearAndPrice(2011, 2000));
        printCar(park.carsOfModel("Avensis"));
        printCar(park.usedCars(5));

    }
}
