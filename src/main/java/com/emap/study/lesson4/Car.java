package com.emap.study.lesson4;

/**
 * Created by Stein on 04.11.16.
 */
public class Car {
    private Long id;
    private String brend;
    private String model;
    private Integer produceYear;
    private String colour;
    private Long price;
    private String registerNumber;

    public Car(Long id, String brend, String model, Integer produceYear, String colour, Long price, String registerNumber) {
        this.id = id;
        this.brend = brend;
        this.model = model;
        this.produceYear = produceYear;
        this.colour = colour;
        this.price = price;
        this.registerNumber = registerNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrend() {
        return brend;
    }

    public void setBrend(String brend) {
        this.brend = brend;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getProduceYear() {
        return produceYear;
    }

    public void setProduceYear(int produceYear) {
        this.produceYear = produceYear;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brend='" + brend + '\'' +
                ", model='" + model + '\'' +
                ", produceYear=" + produceYear +
                ", colour='" + colour + '\'' +
                ", price=" + price +
                ", registerNumber='" + registerNumber + '\'' +
                '}';
    }
}
