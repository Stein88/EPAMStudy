package com.emap.study.lesson4;


import java.util.ArrayList;
import java.util.List;

public class CarPark {
    private List<Car> allCars;
    private static final int CURRENT_YEAR = 2016;

    public CarPark() {
        this.allCars = new ArrayList();
    }

    public List<Car> carsOfModel(String model) {
        List<Car> res = new ArrayList<Car>();
        for (Car car : allCars) {
            if (car.getModel().equals(model)) {
                res.add(car);
            }
        }
        return res;
    }

    public List<Car> usedCars(int year) {
        List<Car> res = new ArrayList<Car>();
        for (Car car : allCars) {
            if ((CURRENT_YEAR - car.getProduceYear()) > year) {
                res.add(car);
            }
        }
        return res;
    }

    public List<Car> carsOfYearAndPrice(int year, int price) {
        List<Car> res = new ArrayList<Car>();
        for (Car car : allCars) {
            if (car.getProduceYear() >= year) {
                if (car.getPrice() >= price) {
                    res.add(car);
                }
            }
        }
        return res;
    }

    public boolean registerCar(List<Car> cars) {
        return allCars.addAll(cars);
    }
}
