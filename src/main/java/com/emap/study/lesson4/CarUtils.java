package com.emap.study.lesson4;

import java.util.List;

/**
 * Created by Stein on 06.11.16.
 */
public class CarUtils {

    public static void printCar(List<Car> cars) {
        for (Car car : cars) {
            System.out.println(car);
        }
    }
}
