package com.emap.study.lesson2;

/**
 * Created by Stein on 01.11.16.
 */
public class Main {
    public static void main(String[] args) {
        Matrix matrix0 = MatrixFactory.createMatrix(3, 4);
        Matrix matrix1 = MatrixFactory.createMatrix(3, 3);
        System.out.println("Matrix 1:");
        System.out.println(matrix0.toString());
        System.out.println("Matrix 2:");
        System.out.println(matrix1.toString());

        try {
            Matrix sumMatrix = Summator.sum(matrix0, matrix1);
            System.out.println("SummMatrix:");
            System.out.println(sumMatrix);
        } catch (Exception e) {
            System.out.println("not equal");
        }
    }
}
