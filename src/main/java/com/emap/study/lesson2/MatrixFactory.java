package com.emap.study.lesson2;

/**
 * Created by Stein on 01.11.16.
 */
public class MatrixFactory {
    private static final int ELEMENT = 20;

    public static Matrix createMatrix(int row, int col) {
        Matrix matrix = new Matrix(row, col);
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix.setElements(i, j, (int) (Math.random() * ELEMENT));
            }
        }
        return matrix;
    }
}
