package com.emap.study.lesson2;

import java.util.Arrays;

/**
 * Created by Stein on 01.11.16.
 */
public class Matrix {
    private int[][] matrix;

    public Matrix(int a, int b) {
        this.matrix = new int[a][b];
    }

    public void setElements(int a, int b, int elem) {
        this.matrix[a][b] = elem;
    }

    public int getElements(int a, int b) {
        return matrix[a][b];
    }

    public int getRowSize() {
        return matrix[0].length;
    }

    public int getColSize() {
        return matrix.length;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (int[] mat : matrix) {
            string.append(Arrays.toString(mat)).append("\n");
        }
        return string.toString();
    }

}
