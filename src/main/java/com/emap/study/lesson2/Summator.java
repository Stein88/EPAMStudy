package com.emap.study.lesson2;

/**
 * Created by Stein on 01.11.16.
 */
public class Summator {
    public static Matrix sum(Matrix matrix0, Matrix matrix1) {
        int matrix0Row = matrix0.getRowSize();
        int matrix0Col = matrix0.getColSize();
        int matrix1Row = matrix1.getRowSize();
        int matrix1Col = matrix1.getColSize();
        if (matrix0Row != matrix1Row || matrix0Col != matrix1Col) {
            throw new RuntimeException();
        }
        Matrix sumMatrix = new Matrix(matrix0Row, matrix0Col);
        for (int i = 0; i < matrix0Row; i++) {
            for (int j = 0; j < matrix0Col; j++) {
                sumMatrix.setElements(i, j, matrix0.getElements(i, j) + matrix1.getElements(i, j));
            }
        }
        return sumMatrix;
    }
}
