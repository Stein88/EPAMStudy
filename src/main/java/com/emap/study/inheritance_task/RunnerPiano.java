package com.emap.study.inheritance_task;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stein on 14.11.16.
 */
public class RunnerPiano {
    public static void main(String[] args) {
        Map<Long, Button> bm = new HashMap<Long, Button>();
        bm.put(1L, new Button("Do"));
        bm.put(2L, new Button("Re"));
        bm.put(3L, new Button("Mi"));
        bm.put(4L, new Button("Fa"));
        bm.put(5L, new Button("Sol"));
        bm.put(6L, new Button("La"));
        bm.put(7L, new Button("Si"));

        Piano piano = new Piano("sevenNotePiano", bm);
        System.out.println(piano);
        System.out.println();
        piano.pressButton(1L);
        System.out.println();
        piano.play(Arrays.asList(5L, 6L, 1L, 2L, 7L, 3L, 4L));

    }
}
