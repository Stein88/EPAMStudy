package com.emap.study.inheritance_task;

/**
 * Created by Stein on 16.11.16.
 */
public  class Button {
    private String sound;

    public Button(String sound) {
        this.sound = sound;
    }

    public void push() {
        System.out.println(toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Button doButton = (Button) o;

        return sound != null ? sound.equals(doButton.sound) : doButton.sound == null;
    }

    @Override
    public int hashCode() {
        return sound != null ? sound.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "sound " + sound;
    }
}
