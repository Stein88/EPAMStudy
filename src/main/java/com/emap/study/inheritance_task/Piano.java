package com.emap.study.inheritance_task;

import java.util.List;
import java.util.Map;

/**
 * Created by Stein on 14.11.16.
 */
public class Piano {
    private String pianoName;
    private Map<Long, Button> bm;

    public Piano(String pianoName, Map<Long, Button> bm) {
        this.pianoName = pianoName;
        this.bm = bm;
    }

    public void pressButton(Long id) {
        Button button = bm.get(id);
        if (button != null) {
            button.push();
        }
    }

    public void play(List<Long> ids) {
        for (Long id : ids) {
            pressButton(id);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Piano piano = (Piano) o;

        if (pianoName != null ? !pianoName.equals(piano.pianoName) : piano.pianoName != null) return false;
        return bm != null ? bm.equals(piano.bm) : piano.bm == null;
    }

    @Override
    public int hashCode() {
        int result = pianoName != null ? pianoName.hashCode() : 0;
        result = 31 * result + (bm != null ? bm.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "pianoName " + pianoName;
    }

//    public void changeStatus(Long id, boolean status) {
//        Button button = bm.get(id);
//        button.setStatus(status);
//    }
//
//    public void removeInActive() {
//        Iterator<Map.Entry<Long, Button>> buttonsIterator = bm.entrySet().iterator();
//        while (buttonsIterator.hasNext()) {
//            Map.Entry<Long, Button> entry = buttonsIterator.next();
//            if (!entry.getValue().getStatus()) {
//                buttonsIterator.remove();
//            }
//        }
//    }
}
